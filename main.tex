\documentclass{scrartcl}
\usepackage[utf8]{inputenc}

\title{Towards Context-Aware Language Models for Historical OCR Post-Correction}
\subtitle{Interim Report}
\author{Robert Sachunsky\footnotemark[1] \and Lena Schiffer\footnotemark[1] \and Thomas Efer\footnotemark[1] \and Gerhard Heyer\footnotemark[1]}
%\date{June 2018}
\date{\today}

%\usepackage{natbib}
%\usepackage{graphicx}

\usepackage{tikz}
\usetikzlibrary{shapes,arrows,graphs,graphdrawing}

\begin{document}

\maketitle
\footnotetext[1]{Universität Leipzig, Informatik, ASV}

\section{Background}

Despite massive digitisation efforts, most of our printed cultural heritage is not yet incorporated into modern research and presentation environments and therefore not accessible by scholars and the public. Our ability to explore, share and create knowledge relies on the broad availability of intellectual content in the form of fully integrated digital resources. The key technological enabler for such rich digital collections is Optical Character Recognition (OCR), the automatic extraction of machine-readable full-text from images. Although OCR quality has improved substantially over the last decade, it still struggles to perform adequately on historic material. In earlier decades of letterpress printing, writing systems were fairly heterogeneous and volatile, posing challenges for standard OCR (which is usually trained on modern examples) that include unknown fonts, glyphs and symbols as well as historic orthography, vocabulary, and layout in conjunction with oftentimes poor visual quality of the old sources.

\section{Problem}

To make large-scale full-text digitisation feasible, those issues need to be addressed fundamentally. They concern mastering images (i.e. pre-processing and pattern recognition) just as much as natural language (i.e. prior knowledge of which textual hypothesis is most likely to make sense). That latter aspect can be framed as stochastic language modelling. As with all statistics, the adequacy of such a model critically depends on both how representative its underlying samples are of the general phenomenon (which in this case is a very productive process), and whether it is able to capture and adapt to any specific \emph{context}. Owing to the vast diversity and complexity of language, context in that sense spans multiple dimensions at different scales: from global ``domain'' meta-data attributes like document type, genre, time and origin to local factors such as region type, form features, font, topic and language (which can switch across quotes, proverbs or single words). These factors will shape the underlying probability distribution, so ideally OCR should condition its approximation model on as many of these factors as possible. 

However, in practice such systems are already quite complex. Moreover, not enough manually corrected historical data have been available for training yet, and it is difficult to unify them across meta-data schemes already. So instead of developing historical models for existing OCR tools, it can be prudent to first build complementary tools for \emph{post-correction} which feed from all visual and contextual cues of OCR itself, but maintain and expand their own comprehensive models. Doing so now also entails modelling errors introduced by OCR besides language itself. 

\section{Aim}

This project is part of the joint coordinated research effort OCR-D \cite{ocrd2018} which is aimed at developing OCR methods for printed historic material in preparation of automated mass digitisation of all German-speaking prints from the 16th to 19th century. To that end, several module projects on various component problems have been devised to examine how research results can be utilised, and to integrate findings and tools into a prototype workflow. 

Our module is dedicated to new tools and models for OCR post-correction. We shall first explore and compare various techniques among the two major modelling paradigms, viz. weighted finite-state transducers (WFST) and artificial neural networks (ANN). Through these experiments a system will be implemented that works well, and can eventually be executed as a module of the encompassing OCR-D pipeline, or run as a standalone application. At its core we shall provide comprehensive standard models which maximally exploit all available training data and meta-data for both historic and contemporary texts, which we thereby expect to handle German-speaking historic prints well.

\section{Methodology}

In the simplest case, OCR post-correction is performed by using a \emph{lexicon} of known word forms and correcting unknown character strings based on shortest edit-distance. However, this approach neglects the rich information sources available: prior knowledge on what mistakes OCR typically makes, how new words are formed grammatically (as described by morphology), and which words are likely to appear next to each other (as studied by syntax and semantics). In a machine learning setting these can be subsumed under stochastic language modelling, which is usually done by either WFST or ANN methods.

\subsection{Automata}

Many existing post-correction approaches employ WFST \cite{tongevans1996,brillmoore2000,kolak2005,llobet2010,alazawibreuel2014,reffleringlstetter2012}, which have a large variety of other applications as they allow for compact storage and processing of sequences of symbols. Since they have been studied extensively, there exist many efficient algorithms and well maintained implementations \cite{allauzen2007,linden2011}. Using WFST, an OCR post-correction system can be modelled as the \emph{composition} of single transducers representing input character hypotheses, error model, lexicon, and word-level language model, each weighted with probabilities based on counts observed in the training data. The result is a transducer containing all possible correction candidates alongside their probabilities.

Such a system obviously benefits strongly from receiving multiple \emph{character hypotheses} accompanied by confidence values as input instead of the prevalent single-best full line result, implying a tighter integration with OCR proper. 
Such confidences cannot be translated into weights at face value though, or else the correction will be biased again towards the OCR models.

Due to the rich morphology in synthetic languages like German, a mere list of word forms does not in general suffice as word model. We will extend the lexicon transducer to become a \emph{dynamic} word model by composing it with a transducer trained to approximate rules for inflection, derivation, and composition of lexemes.

One of the most common errors in OCR are word segmentation errors, i.e. errors including whitespace characters. Hence, the correction should be allowed to search across multiple words at once. Since the WFST approach is unable to handle long input passages efficiently, we will implement a \emph{sliding window} technique, recombining window outputs afterwards. Moreover, when sliding across line boundaries, special interim symbols can be introduced to mark that context and hide hyphens from the word model.

\subsection{Neural networks}

More recently, the fast-paced advances of deep-learning methods and their overwhelming success in a broad range of applications have spurred researchers on proposing new post-correction approaches based on ANN \cite{alazawi2014,dhondt2017,dongsmith2018}. Neural networks do not only offer more efficient predictions which require no limitations on the number of errors per word (because they do not influence memory requirements) or the length of sequences to consider (as they suffer far less from data sparsity). ANN are also strictly more powerful (when including recurrence and non-linearity), while offering very good practical generalisation performance (the causes of which are slowly becoming understood analytically at last \cite{geman1992,barron1994,bengio1994,lee2017,zhang2017,advanisaxe2017})).

\subsubsection{Byte-level encoder-decoder}

Of particular attraction (and now the predominant paradigm in machine translation) is the \emph{encoder-decoder} model for sequence-to-sequence transduction with beam search \cite{cho2014,sutskever2014} (often augmented by attention \cite{bahdanau2014,vaswani2017} and protected against exposure bias and label bias \cite{wisemanrush2016,lamb2016}). As with language modelling in general, it can be applied on the level of words (which depends on a good segmentation and handling of unknown tokens) or characters (where it is more difficult to account for differences of meaning across longer distances). They can be combined in many ways, both implicitly \cite{chung2016} and explicitly \cite{kim2015,luongmanning2016}, or even successively (by using a word level model to re-score the search space of a character level model, see below). For historic texts though, with frequent use of Greek and Hebrew scripts, and a large number of rare graphemes including combining diacritical marks, descending as far as on the \emph{byte level} of a UTF-8 transport stream is not only more reliable, but also helps to keep models compact \cite{gillick2015}. In addition, by employing encoder-decoder models statefully, unless they contain hidden layers operating backwards (as would BLSTM \cite{graves-schmidhuber2005}), they can be run as sliding window, too (both with or without attention).

\begin{figure}[t]
  \resizebox{15.5cm}{!}{%
  \begin{tikzpicture}[
  hid/.style 2 args={
    rectangle split,
    rectangle split,
    draw=#2,
    rectangle split parts=#1,
    fill=#2!20,
    outer sep=1mm}]
  \usetikzlibrary{calc}
  % show window rectangle
  \draw[gray,fill=gray!10,fill opacity=0.7,text opacity=1] (1.5,-8) rectangle (10.5,-5.5);
  \node (iwindow) at (6, -7) {encoder window=5};
  % draw input nodes
  \foreach \i [count=\step from 0] in {\textvisiblespace,m,i,ü,s,s,e,n,w,i,r,k,l,i}
    \node (i\step) at (2*\step, -6) {\i};
  % show window rectangles
  \draw[gray,fill=gray!10,fill opacity=0.7,text opacity=1] (11.5,5.5) rectangle (22.5,7.75);
  \node (owindow) at (17, 6.75) {decoder window=6};
  % draw output nodes
  \foreach \t [count=\step from 0] in {\textvisiblespace,d,a,s,\textvisiblespace,$\varepsilon$,m,ü,s,s,$\varepsilon$,$\varepsilon$,e,n} {
    \node[align=center] (o\step) at (2*\step, +5.75) {\t};
  }
  % draw embedding and hidden layers for text input
  \foreach \step in {1,...,5} {
    \node[hid={3}{red}] (h\step) at (2*\step, -2) {};
    \node[hid={4}{red}] (e\step) at (2*\step, -4) {};    
    \node               (a\step) at (2*\step, 1) {};    
    \draw[->] (i\step.north) -> (e\step.south);
    \draw[->] (e\step.north) -> (h\step.south);
    \draw[->] (h\step.north) -> (a\step.south);
  }
  % draw embedding and hidden layers for label input
  \foreach \step in {6,...,11} {
    \node[hid={4}{yellow}] (s\step) at (2*\step, 4.25) {};
    \node[hid={3}{blue}] (h\step) at (2*\step, 2) {};
    \node[hid={4}{blue}] (e\step) at (2*\step, 0) {};    
    \draw[->] (e\step.north) -> (h\step.south);
    \draw[->] (h\step.north) -> (s\step.south);
    \draw[->] (s\step.north) -> (o\step.south);
  }  
  % draw recurrent links
  \foreach \step in {1,...,4} {
    \pgfmathtruncatemacro{\next}{add(\step,1)}
    \draw[->] (h\step.east) -> (h\next.west);
  }
  \path (h5.east) edge[->,dashed,out=45,in=225] (h1.west);
  \foreach \step in {6,...,10} {
    \pgfmathtruncatemacro{\next}{add(\step,1)}
    \draw[->] (h\step.east) -> (h\next.west);
  }
  \path (h11.east) edge[->,dashed,out=45,in=225] (h6.west);
  % draw predicted-labels-as-inputs links
  \foreach \step in {6,...,10} {
    \pgfmathtruncatemacro{\next}{add(\step,1)}
    \path (o\step.north) edge[->,out=45,in=225] (e\next.south);
  }
  % edge case: draw edge for special input token
  \node (h11post) at (2*11+1, 2) {};
  \path (o11.north) edge[->,dashed,out=45,in=90] (h11post);
  \node (h6pre) at (2*6-1, 2) {};
  \path (h6pre) edge[->,dashed,out=90,in=225] (e6.south);
  % attention/peeking box for transfer
  \draw (1.5,1) rectangle (10.5,3);
  \node (attention) at (6,2) {attention / peeking};
  \foreach \step in {6,...,11} {
    \path (6,3) edge[->,out=73,in=107,looseness=0.4] (h\step);
  }
  % beam search box?
  % context variable input and context embeddings?
  \end{tikzpicture}%
  }
  \caption{sliding window with stateful encoder-decoder}
\end{figure}  

\subsubsection{Error modelling and context modelling}

For estimation of the OCR error model from a finite sample, most WFST approaches use some form of supervised learning (exploiting manually corrected data, which is not only too expensive but also hard to acquire in a way representative of all contexts). Unsupervised learning via Expectation Maximisation has been proven effective as well – both inductively \cite{tongevans1996} and transductively \cite{reffleringlstetter2012}. However, the available data on all kinds of context still have to be split into a finite number of discrete, static, incomparable \emph{domains}, which can be argued to be an ill-defined, reductionist notion \cite{ruder2016}. 

With ANN, since models can be pre-trained on auxiliary tasks, it is easy to learn a single inductive model both on labelled and unlabelled data (i.e. semi-supervised). Moreover, this model can be made \emph{context-aware} by feeding all context variables as additional input and having the network learn an implicit lower-dimensional representation (context embedding) which dynamically combines with all other components and thus makes it adaptive \cite{jaechostendorf2017,yogatama2017}. 

\subsubsection{Single model, multiple data}

Applied to the encoder-decoder post-correction model, which is trained to reduce the reconstruction loss (effectively learning an error model in the encoder and a conditional language model in the decoder), this means we can do \emph{supervised training} by presenting
\begin{itemize}
    \item OCR output to the encoder and ground truth text to the decoder,
\end{itemize} while also doing \emph{unsupervised training} by presenting 
\begin{itemize}
\item clean texts to both sides, possibly keeping encoder weights fixed,
\item noisy OCR texts to both sides, while keeping decoder weights fixed, and even
\item clean text to the decoder, but randomly degraded text to the encoder \cite{dhondt2017}, possibly keeping decoder weights fixed.
\end{itemize}
In doing so, the encoder input side can be made to resemble OCR output either as single-best results or confidence-backed character hypotheses. In the latter case, ANN would presumably not be as susceptible to becoming biased by OCR as WFST are. And for degradation, apart from introducing hard errors by replacing, removing and inserting characters, one can also consider merely adding soft noise to the confidence values.

Presented textual data can be both historic and contemporary, as long as they are augmented with sufficiently differentiating contextual meta-data: Both encoder and decoder can be conditioned on these \emph{context variables} by adding and multiplying their embeddings to hidden and output layers \cite{jaechostendorf2017}. Additionally, in order to make maximal use of context variables (some of which we a priori know to be drawn from non-stationary processes of language change, e.g. time and local origin), the loss can be complemented by constraints on smoothness and directedness of context embeddings \cite{hoffman2014,hamilton2016}. 

\subsubsection{Underspecification vs. lazy classification}

Of course, not all context values will be known completely for all available samples. As to that, we can either try using \emph{underspecification} (with zero), or (at least during inference) feeding each possible value into one parallel copy of the model and keep only that hypothesis which scores best consistently over some segment (switching only at certain character hypotheses like whitespace or changes in other variables like script or shape). This lazy \emph{generative classification} \cite{yogatama2017} strategy will probably be most worthwhile for code switches in language, but could also be used to detect named entities (perhaps by simply treating them as yet another language).

\subsubsection{Parallel byte-level and word-level encoder-decoders}

Regarding the possibility of successive application of a \emph{word-based} correction to re-score hypotheses generated by the byte-level model, a straightforward approach would be using a \emph{character-compositional} word-level language model \cite{ling2015,vania2017} (comprising an encoder as word model and a decoder as language model). The size of the second decoder's vocabulary will strongly determine run-time and memory requirements (softmax effort and encoder cache hit rate), as well as accuracy (how much remaining probability mass needs to be evenly distributed among unknown words and non-words).

Both encoder-decoder models will now run in parallel sharing \emph{one search beam}, which is a tree of $k$ running hypotheses fed from alternatives of both the context classification input and the first decoder's output. For each current hypothesis, the second encoder's input will be its history's byte sequence up to the last whitespace byte, and the second decoder's input will be its history's word sequence before that point. At each whitespace byte then, 
\begin{itemize}
    \item current context classification will cede to a full set of new input alternatives,
    \item the current hypothesis' history up to the last whitespace will be re-scored by the previous prediction of the second decoder for that word,
    \item the second decoder will make new predictions based on the second encoder's state (which is then reset).
\end{itemize}
Importantly, the window size of the second encoder-decoder needs to be much larger than that of the first, and  depends on how it is trained (statefully or on segments like lines, sentences or paragraphs). Also, the tokenisation scheme applied here must match the pre-processing of the text corpus. Context variables should be supplied to the second encoder-decoder as well. If the word-level model does not get to see enough training data to obtain good word embeddings, its encoder can also be pre-trained on large n-gram counts \cite{ginter-kanerva2014,hamilton2016}, or be initialised on existing word embeddings \cite{pinter2017}.

\subsection{Hybrids}

Finally, both methods might even benefit each other. The ANN's context embeddings could be extracted as a domain classifier to select distinct WFST models. Conversely, the WFST's predictions could become additional inputs to the ANN. One can also multiplex both system's outputs segment-wise based on respective confidences.





\bibliographystyle{plain}
\bibliography{references}
\end{document}
